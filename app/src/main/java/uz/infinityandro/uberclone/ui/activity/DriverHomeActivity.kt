package uz.infinityandro.uberclone.ui.activity

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.bumptech.glide.Glide
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import de.hdodenhof.circleimageview.CircleImageView
import uz.infinityandro.uberclone.R
import uz.infinityandro.uberclone.databinding.ActivityDriverHomeBinding
import uz.infinityandro.uberclone.util.Common
import uz.infinityandro.uberclone.util.UserUtil
import uz.infinityandro.uberclone.util.displayToast
import java.lang.StringBuilder

class DriverHomeActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityDriverHomeBinding
    private lateinit var img_avatar: ImageView
    private lateinit var imageUri: Uri
    private lateinit var waitingDialog: AlertDialog
    private lateinit var storageReference: StorageReference
    private lateinit var drawerLayout:DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDriverHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarDriverHome.toolbar)
        drawerLayout=findViewById<DrawerLayout>(R.id.drawer_layout)
        val navView: NavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_driver_home)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


            storageReference = FirebaseStorage.getInstance().getReference()
            dialog()

        navView.setNavigationItemSelectedListener { menu ->
            navDialog(menu)
            true
        }


        val header = navView.getHeaderView(0)
        val name = header.findViewById<AppCompatTextView>(R.id.txt_name)
        val phone = header.findViewById<AppCompatTextView>(R.id.txt_phone)
        val star = header.findViewById<AppCompatTextView>(R.id.txt_star)
        img_avatar = header.findViewById<CircleImageView>(R.id.imageView)
        name.setText(Common.buildWelcomeMessage())
        phone.setText(Common.currentUser!!.phoneNumber)
        star.setText(Common.currentUser!!.rating.toString())
        if (Common.currentUser != null && Common.currentUser!!.image != null && !TextUtils.isEmpty(
                Common.currentUser!!.image
            )
        ) {
            Glide.with(this).load(Common.currentUser!!.image).into(img_avatar)

        }
        img_avatar.setOnClickListener {
            val intent = Intent()
            intent.setType("image/*")
            intent.setAction(Intent.ACTION_GET_CONTENT)
            startActivityForResult(
                Intent.createChooser(intent, "Select Picture"),
                PICK_IMAGE_REQUEST
            )
        }
    }


    private fun navDialog(menu: MenuItem) {
        if (menu.itemId == R.id.nav_signout) {
            val builder = AlertDialog.Builder(this).create()
            builder.setTitle("Sign out")
            builder.setMessage("Do you really want to sign out?")
            builder.setButton(
                AlertDialog.BUTTON_NEGATIVE,
                "Cancel",
                DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                })
            builder.setButton(AlertDialog.BUTTON_POSITIVE, "Sign out") { dialog, t ->
                FirebaseAuth.getInstance().signOut()
                Intent(this, SplashScreenActivity::class.java).let {
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(it)
                    finish()
                    dialog.dismiss()
                }

            }
            builder.setOnShowListener {
                builder.getButton(AlertDialog.BUTTON_POSITIVE)
                    .setTextColor(resources.getColor(android.R.color.holo_red_dark))
                builder.getButton(AlertDialog.BUTTON_NEGATIVE)
                    .setTextColor(resources.getColor(android.R.color.black))
            }
            builder.show()
        }
        if (menu.itemId==R.id.nav_home){
            if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START)
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }
    }

    private fun dialog() {
        waitingDialog = AlertDialog.Builder(this)
            .setMessage("Waiting...")
            .setCancelable(false).create()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.driver_home, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_driver_home)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    companion object {
        val PICK_IMAGE_REQUEST = 7272
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data != null && data.data != null) {
                imageUri = data.data!!
                img_avatar.setImageURI(imageUri)
                showDialogToUpload()
            }
        }
    }

    private fun showDialogToUpload() {
        val builder = AlertDialog.Builder(this).create()
        builder.setTitle("Change Avatar")
        builder.setMessage("Do you really want to change Avatar?")
        builder.setButton(
            AlertDialog.BUTTON_NEGATIVE,
            "No",
            DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
            })
        builder.setButton(AlertDialog.BUTTON_POSITIVE, "Change") { dialog, t ->
            if (imageUri != null) {
                waitingDialog.show()
                val avatarFolder =
                    storageReference.child("avatars/" + FirebaseAuth.getInstance().currentUser!!.uid)
                avatarFolder.putFile(imageUri)
                    .addOnSuccessListener {

                    }.addOnFailureListener {
                        displayToast("Error")
                        waitingDialog.dismiss()
                    }
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            avatarFolder.downloadUrl.addOnSuccessListener {
                            val update_data= hashMapOf<String,Any>()
                                update_data.put("avatars",it.toString())
                                UserUtil.updateUser(drawerLayout,update_data)
                            }
                        }
                        waitingDialog.dismiss()
                    }.addOnProgressListener {task->
                    val process=(100.0*task.bytesTransferred/task.totalByteCount)
                        waitingDialog.setMessage(StringBuilder("Uploading: ").append(process).append("%"))

                    }

            }
        }
        builder.setOnShowListener {
            builder.getButton(AlertDialog.BUTTON_POSITIVE)
                .setTextColor(resources.getColor(android.R.color.holo_red_dark))
            builder.getButton(AlertDialog.BUTTON_NEGATIVE)
                .setTextColor(resources.getColor(android.R.color.black))
        }
        builder.show()
    }
}