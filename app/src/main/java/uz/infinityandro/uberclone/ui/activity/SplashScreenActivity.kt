package uz.infinityandro.uberclone.ui.activity

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.AppCompatButton
import com.firebase.ui.auth.AuthMethodPickerLayout
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.iid.FirebaseInstanceIdReceiver
import com.google.firebase.iid.internal.FirebaseInstanceIdInternal
import com.google.firebase.installations.FirebaseInstallations
import com.google.firebase.installations.FirebaseInstallationsApi
import com.google.firebase.messaging.FirebaseMessaging
import uz.infinityandro.uberclone.R
import uz.infinityandro.uberclone.model.DriverInfoModel
import uz.infinityandro.uberclone.util.Common
import uz.infinityandro.uberclone.util.UserUtil
import uz.infinityandro.uberclone.util.displayToast
import java.util.*

class SplashScreenActivity : AppCompatActivity() {

    companion object {
        private val LOGIN_REQUEST_CODE = 7171
    }

    private lateinit var providers: List<AuthUI.IdpConfig>
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var listener: FirebaseAuth.AuthStateListener
    private lateinit var database: FirebaseDatabase
    private lateinit var driverInfoRef: DatabaseReference


    //onCreate
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_progress)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        init()
    }


    //onStart
    override fun onStart() {
        super.onStart()
        delaySplashScreen()
    }


    //Splash oynani delay qiladi
    private fun delaySplashScreen() {
        Handler(Looper.myLooper()!!).postDelayed(object : Runnable {
            override fun run() {
                firebaseAuth.addAuthStateListener(listener)
            }

        }, 3000)
    }


    //onStop
    override fun onStop() {
        if (firebaseAuth != null && listener != null) firebaseAuth.removeAuthStateListener(listener)
        super.onStop()

    }

    //init function
    private fun init() {
        database = FirebaseDatabase.getInstance()
        driverInfoRef = database.getReference(Common.DRIVER_INFO_REFERENCE)
        providers = Arrays.asList(
            AuthUI.IdpConfig.PhoneBuilder().build(),
            AuthUI.IdpConfig.GoogleBuilder().build()

        )
        firebaseAuth = FirebaseAuth.getInstance()
        listener = FirebaseAuth.AuthStateListener { myListener ->
            val user = myListener.currentUser
            if (user != null) {
                FirebaseMessaging.getInstance()
                    .token
                    .addOnFailureListener {e->
                    displayToast("${e.message}")
                    }.addOnSuccessListener { id->
                        Log.d("TAG", "init: $id")
                        UserUtil.updateToken(this,id)
                    }.addOnCompleteListener {

                    }
                chekUserFromFirebase()
            } else {
                showLoginLayout()
            }
        }
    }


    //chekUserFromFirebase
    private fun chekUserFromFirebase() {
        driverInfoRef.child(FirebaseAuth.getInstance().currentUser!!.uid)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        val model = snapshot.getValue(DriverInfoModel::class.java)
                        goToHomeActivity(model)
                    } else {
                        showRegisterLayout()
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    displayToast("${error.message}")
                }

            })
    }

    private fun goToHomeActivity(model: DriverInfoModel?) {
        Common.currentUser = model
        startActivity(Intent(this, DriverHomeActivity::class.java))
        finish()
    }

    private fun showRegisterLayout() {
        val builder = AlertDialog.Builder(this, R.style.DialogTheme)
        val itemView = LayoutInflater.from(this).inflate(R.layout.layout_register, null)

        val name = itemView.findViewById<View>(R.id.inputName) as TextInputEditText
        val lastName = itemView.findViewById<View>(R.id.inputLastName) as TextInputEditText
        val phoneNumber = itemView.findViewById<View>(R.id.inputPhoneNumber) as TextInputEditText
        val btn = itemView.findViewById<AppCompatButton>(R.id.buttonContinue)
        val progresBar = findViewById<ProgressBar>(R.id.progress_bar)
        if (FirebaseAuth.getInstance().currentUser?.phoneNumber != null && !TextUtils.isDigitsOnly(
                FirebaseAuth.getInstance().currentUser?.phoneNumber
            )
        )
            phoneNumber.setText(FirebaseAuth.getInstance().currentUser?.phoneNumber)

        builder.setView(itemView)
        val dialog = builder.create()
        dialog.show()

        btn.setOnClickListener {
            if (TextUtils.isDigitsOnly(name.text.toString())) {
                displayToast("Please enter First Name")
                return@setOnClickListener
            } else if (TextUtils.isDigitsOnly(lastName.text.toString())) {
                displayToast("Please enter Last Name")
                return@setOnClickListener
            } else if (TextUtils.isDigitsOnly(phoneNumber.text.toString())) {
                displayToast("Please enter Phone Number")
                return@setOnClickListener
            } else {
                val model = DriverInfoModel()
                model.lastName = lastName.text.toString()
                model.name = name.text.toString()
                model.phoneNumber = phoneNumber.text.toString()
                model.rating = 0.0

                driverInfoRef.child(FirebaseAuth.getInstance().currentUser!!.uid)
                    .setValue(model)
                    .addOnSuccessListener {
                        displayToast("Successful Register")
                        dialog.dismiss()
                        progresBar.visibility = View.GONE
                        goToHomeActivity(model)
                    }
                    .addOnFailureListener {

                        dialog.dismiss()
                        progresBar.visibility = View.GONE


                    }
            }
        }
    }

    private fun showLoginLayout() {
        val authMethodPickerLayout = AuthMethodPickerLayout.Builder(R.layout.splash_screen_layout)
            .setPhoneButtonId(R.id.buttonSignPhone)
            .setGoogleButtonId(R.id.buttonSignInGoogle)
            .build()
        startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAuthMethodPickerLayout(authMethodPickerLayout)
                .setAvailableProviders(providers)
                .setTheme(R.style.ThemeSplash)
                .setIsSmartLockEnabled(false)
                .build(), LOGIN_REQUEST_CODE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == LOGIN_REQUEST_CODE) {
            val response = IdpResponse.fromResultIntent(data)
            if (resultCode == Activity.RESULT_OK) {
                val user = FirebaseAuth.getInstance().currentUser
            } else {
                Toast.makeText(
                    applicationContext,
                    "" + response!!.error!!.message!!,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}