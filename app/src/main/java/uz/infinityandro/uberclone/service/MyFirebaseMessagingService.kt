package uz.infinityandro.uberclone.service

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import uz.infinityandro.uberclone.util.Common
import uz.infinityandro.uberclone.util.UserUtil
import kotlin.random.Random

class MyFirebaseMessagingService:FirebaseMessagingService(){
    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        if (FirebaseAuth.getInstance().currentUser!=null){
            UserUtil.updateToken(this,p0)
        }
    }

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        val data=p0.data
        if (data!=null){
            Common.showNotification(this,Random.nextInt(),
            data[Common.NOTI_TITLE],
            data[Common.NOTI_BODY],
            null)
        }

    }
}