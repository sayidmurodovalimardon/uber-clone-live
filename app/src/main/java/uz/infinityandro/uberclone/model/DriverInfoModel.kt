package uz.infinityandro.uberclone.model

class DriverInfoModel {
    var name: String = ""
    var lastName: String = ""
    var phoneNumber: String = ""
    var image:String=""
    var rating = 0.0

}