package uz.infinityandro.uberclone.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import androidx.core.app.NotificationCompat
import uz.infinityandro.uberclone.R
import uz.infinityandro.uberclone.model.DriverInfoModel

object Common {

    fun buildWelcomeMessage(): String {
        return StringBuilder("Welcome, ")
            .append(currentUser!!.name)
            .append(" ")
            .append(currentUser!!.lastName)
            .toString()

    }

    fun showNotification(
        context: Context,
        id: Int,
        title: String?,
        body: String?,
        intent: Intent?
    ) {
        var pendingIntent:PendingIntent?=null
        if (intent!=null){
            pendingIntent= PendingIntent.getActivity(context,id,intent,PendingIntent.FLAG_UPDATE_CURRENT)
            val NOTIFICATION_CHANNEL_ID="ali_dev"
            val notificaManager=context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
                val notificationChannel=NotificationChannel(NOTIFICATION_CHANNEL_ID,"Uber Remake",
                NotificationManager.IMPORTANCE_HIGH)
                notificationChannel.description="Uber Remake"
                notificationChannel.enableLights(true)
                notificationChannel.lightColor=Color.RED
                notificationChannel.vibrationPattern= longArrayOf(0,1000,500,1000)
                notificationChannel.enableVibration(true)
                notificaManager.createNotificationChannel(notificationChannel)
            }
            val builder=NotificationCompat.Builder(context,NOTIFICATION_CHANNEL_ID)
            builder.setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(false)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                .setSmallIcon(R.drawable.ic_baseline_directions_car_24)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources,R.drawable.ic_baseline_directions_car_24))
            if (pendingIntent!=null){
                builder.setContentIntent(pendingIntent)
                val notification=builder.build()
                notificaManager.notify(id, notification)
            }

        }

    }


    val NOTI_BODY: String = "body"
    val NOTI_TITLE: String = "title"
    const val TOKEN_REFERNCE = "Token"
    const val DRIVERS_LOCATION_REFERENCE = "DriversLocation"
    var currentUser: DriverInfoModel? = null
    const val DRIVER_INFO_REFERENCE = "DriverInfo"
}