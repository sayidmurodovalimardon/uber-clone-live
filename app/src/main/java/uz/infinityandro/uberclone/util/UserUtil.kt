package uz.infinityandro.uberclone.util

import android.content.Context
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import uz.infinityandro.uberclone.model.TokenModel
import uz.infinityandro.uberclone.service.MyFirebaseMessagingService

object UserUtil {
    fun updateUser(
        view: View?,
        updateData: HashMap<String, Any>
    ) {
        FirebaseDatabase.getInstance()
            .getReference(Common.DRIVER_INFO_REFERENCE)
            .child(FirebaseAuth.getInstance().currentUser!!.uid)
            .updateChildren(updateData)
            .addOnFailureListener {
                Snackbar.make(view!!,"${it.message}",Snackbar.LENGTH_LONG).show()
            }.addOnSuccessListener {
                Snackbar.make(view!!,"Update Information Success",Snackbar.LENGTH_LONG).show()

            }
    }

    fun updateToken(context: Context, p0: String) {
        var tokenModel=TokenModel()
        tokenModel.token=p0
        FirebaseDatabase.getInstance().getReference(Common.TOKEN_REFERNCE)
            .child(FirebaseAuth.getInstance().currentUser!!.uid)
            .setValue(p0)
            .addOnFailureListener {

            }.addOnSuccessListener {

            }
    }
}